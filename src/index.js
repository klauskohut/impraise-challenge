import React, { Component } from "react";
import ReactDOM from "react-dom";
import { ApolloProvider } from "react-apollo";
import styled, { injectGlobal } from "styled-components";

import OrganizationInfo from "./shared/components/OrganizationInfo";
import AllRepositories from "./shared/components/AllRepositories.js";
import PinnedRepositories from "./shared/components/PinnedRepositories.js";
import { client } from "./apollo/index.js";

injectGlobal`
  body {
    margin: 0;
  }
`;

const Wrapper = styled.div`
  display: flex;
  flex-flow: column;
`;

class App extends Component {
  state = {
    login: "Impraise"
  };

  render() {
    return (
      <Wrapper>
        <ApolloProvider client={client}>
          <OrganizationInfo login={this.state.login} />
          <PinnedRepositories login={this.state.login} />
          <AllRepositories login={this.state.login} />
        </ApolloProvider>
      </Wrapper>
    );
  }
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
