import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const Text = styled.span`
  display: flex;
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica,
    Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
  color: ${props => props.color};
  font-size: ${props => props.size}px;
  line-height: ${props => props.size * 1.6}px;
  align-items: center;
`;

Text.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.string.isRequired
};

export default props => <Text {...props}>{props.children}</Text>;
