import React, { Fragment } from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";

import Profile from "./Profile";

const GET_REPOS = gql`
  query OrganizationInfo($login: String!) {
    organization(login: $login) {
      name
      location
      websiteUrl
    }
  }
`;

const PinnedRepositories = props => {
  return (
    <Query
      query={GET_REPOS}
      variables={{
        login: props.login
      }}
    >
      {({ loading, error, data, fetchMore }) => {
        if (loading) return "Loading...";
        if (error) return `Error! ${error.message}`;

        return (
          <Fragment>
            <Profile data={data.organization} />
          </Fragment>
        );
      }}
    </Query>
  );
};

export default PinnedRepositories;
