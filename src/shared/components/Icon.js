import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

import fork from "../assets/git-branch.svg";
import link from "../assets/link.svg";
import law from "../assets/law.svg";
import location from "../assets/location.svg";
import stargazer from "../assets/star.svg";

const imgs = {
  law,
  fork,
  link,
  location,
  stargazer
};

const Wrapper = styled.div`
  display: flex;
  width: 18px
  align-items: center;
  justify-content: center;
  opacity: .4
`;

const Icon = props => {
  return (
    <Wrapper>
      <img src={imgs[props.type]} alt={props.type} />
    </Wrapper>
  );
};

Icon.propTypes = {
  type: PropTypes.string.isRequired
};

export default Icon;
