import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import Box from "./Box";
import Text from "./Text";
import Icon from "./Icon";
import Logo from "./Logo";

const Link = styled.a`
  &:hover > span {
    color: #71b1f5;
  }
  text-decoration: none;
`;

const InnerWrapper = styled.div`
  display: flex;
  flex-flow: column;
  padding: 10px;
`;

const Profile = ({ data }) => (
  <Box paddingLeft="10" paddingRight="10" paddingTop="20" paddingBottom="20">
    <InnerWrapper>
      <Logo name={data.name} />
    </InnerWrapper>
    <InnerWrapper>
      <Text size="32" color="#000">
        {data.name}
      </Text>
      {data.location ? (
        <Text size="14" color="#758293">
          <Icon type="location" />
          &nbsp;
          {data.location}
        </Text>
      ) : null}
      {data.websiteUrl ? (
        <Link href={data.websiteUrl} alt="Go to Url">
          <Text size="14" color="#758293">
            <Icon type="link" />
            &nbsp;
            {data.websiteUrl}
          </Text>
        </Link>
      ) : null}
    </InnerWrapper>
  </Box>
);

Text.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string.isRequired,
    location: PropTypes.string,
    websiteUrl: PropTypes.string
  })
};

export default Profile;
