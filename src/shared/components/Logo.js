import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const Image = styled.img`
  height: 100px;
  width: 100px;
`;

const Logo = props => {
  return (
    <Image
      src="https://avatars0.githubusercontent.com/u/7567947?s=200&v=4"
      alt={props.name}
    />
  );
};

Image.propTypes = {
  name: PropTypes.string
};

export default Logo;
