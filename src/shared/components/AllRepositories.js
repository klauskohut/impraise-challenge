import React, { Fragment } from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";

import Box from "./Box";
import List from "./List";
import Text from "./Text";
import RepositoryListItem from "./RepositoryListItem";

const GET_REPOS = gql`
  query OrganizationInfo($login: String!, $cursor: String, $first: Int!) {
    organization(login: $login) {
      repositories(first: $first, after: $cursor) {
        pageInfo {
          endCursor
          hasNextPage
        }
        edges {
          cursor
          node {
            name
            description
            licenseInfo {
              name
            }
            languages(first: 1) {
              edges {
                node {
                  name
                  color
                }
              }
            }
            stargazers {
              totalCount
            }
            forks {
              totalCount
            }
            updatedAt
            url
          }
        }
      }
    }
  }
`;

const fetchNextRepositories = (fetchMore, data) => {
  fetchMore({
    variables: {
      cursor: data.organization.repositories.pageInfo.endCursor
    },
    updateQuery: (previousResult, { fetchMoreResult }) => {
      const newEdges = fetchMoreResult.organization.repositories.edges;
      const pageInfo = fetchMoreResult.organization.repositories.pageInfo;

      return newEdges.length
        ? {
            organization: {
              __typename: previousResult.organization.__typename,
              repositories: {
                __typename: previousResult.organization.repositories.__typename,
                edges: [
                  ...previousResult.organization.repositories.edges,
                  ...newEdges
                ],
                pageInfo
              }
            }
          }
        : previousResult;
    }
  });
};

const AllRepositories = props => {
  return (
    <Query query={GET_REPOS} variables={{ login: props.login, first: 10 }}>
      {({ loading, error, data, fetchMore }) => {
        if (loading) return "Loading...";
        if (error) return `Error! ${error.message}`;

        // Not really the best way, but in the exercise disclaimer asks for us to
        // grab all the repositories
        if (data.organization.repositories.pageInfo.hasNextPage) {
          fetchNextRepositories(fetchMore, data);
        }

        if (!data.organization.repositories.edges.length) return null;

        return (
          <Fragment>
            <Box
              paddingLeft="20"
              paddingRight="20"
              paddingTop="20"
              paddingBottom="10"
            >
              <Text size="24" color="#666">
                Repositories
              </Text>
            </Box>
            <List direction="column">
              {data.organization.repositories.edges.map((edge, index) => {
                const {
                  name,
                  updatedAt,
                  description,
                  licenseInfo,
                  languages,
                  stargazers,
                  forks,
                  url
                } = edge.node;
                return (
                  <RepositoryListItem
                    key={`allRepoItem${index}`}
                    description={description}
                    forksCount={forks.totalCount}
                    license={licenseInfo ? licenseInfo.name : null}
                    language={
                      languages.edges.length ? languages.edges[0].node : null
                    }
                    name={name}
                    stargazersCount={stargazers.totalCount}
                    updatedAt={updatedAt}
                    url={url}
                    type="listItem"
                  />
                );
              })}
            </List>
          </Fragment>
        );
      }}
    </Query>
  );
};

export default AllRepositories;
