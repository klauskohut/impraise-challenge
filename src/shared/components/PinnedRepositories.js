import React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";

import Box from "./Box";
import List from "./List";
import Text from "./Text";
import RepositoryListItem from "./RepositoryListItem";

const GET_REPOS = gql`
  query OrganizationInfo($login: String!, $cursor: String, $first: Int!) {
    organization(login: $login) {
      pinnedRepositories(first: $first, after: $cursor) {
        pageInfo {
          endCursor
          hasNextPage
        }
        edges {
          cursor
          node {
            name
            description
            languages(first: 1) {
              edges {
                node {
                  name
                  color
                }
              }
            }
            stargazers {
              totalCount
            }
            forks {
              totalCount
            }
            url
          }
        }
      }
    }
  }
`;

const fetchNextRepositories = (fetchMore, data) => {
  fetchMore({
    variables: {
      cursor: data.organization.pinnedRepositories.pageInfo.endCursor
    },
    updateQuery: (previousResult, { fetchMoreResult }) => {
      const newEdges = fetchMoreResult.organization.pinnedRepositories.edges;
      const pageInfo = fetchMoreResult.organization.pinnedRepositories.pageInfo;

      return newEdges.length
        ? {
            organization: {
              __typename: previousResult.organization.__typename,
              pinnedRepositories: {
                __typename:
                  previousResult.organization.pinnedRepositories.__typename,
                edges: [
                  ...previousResult.organization.pinnedRepositories.edges,
                  ...newEdges
                ],
                pageInfo
              }
            }
          }
        : previousResult;
    }
  });
};

const AllRepositories = props => {
  return (
    <Query query={GET_REPOS} variables={{ login: props.login, first: 10 }}>
      {({ loading, error, data, fetchMore }) => {
        if (loading) return "Loading...";
        if (error) return `Error! ${error.message}`;

        // Not really the best way, but in the exercise disclaimer asks for us to
        // grab all the repositories
        if (data.organization.pinnedRepositories.pageInfo.hasNextPage) {
          fetchNextRepositories(fetchMore, data);
        }

        if (!data.organization.pinnedRepositories.edges.length) return null;

        return (
          <Box
            paddingLeft="20"
            paddingRight="20"
            paddingTop="20"
            paddingBottom="20"
          >
            <List direction="column">
              <Box marginBottom="10">
                <Text size="24" color="#666">
                  Pinned Repositories
                </Text>
              </Box>
              <List direction="row">
                {data.organization.pinnedRepositories.edges.map(
                  (edge, index) => {
                    const {
                      name,
                      languages,
                      description,
                      stargazers,
                      forks,
                      url
                    } = edge.node;
                    return (
                      <RepositoryListItem
                        key={`pinnedItem${index}`}
                        description={description}
                        forksCount={forks.totalCount}
                        name={name}
                        language={
                          languages.edges.length
                            ? languages.edges[0].node
                            : null
                        }
                        stargazersCount={stargazers.totalCount}
                        url={url}
                        type="card"
                      />
                    );
                  }
                )}
              </List>
            </List>
          </Box>
        );
      }}
    </Query>
  );
};

export default AllRepositories;
