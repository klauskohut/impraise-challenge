import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const Wrapper = styled.div`
  display: flex;
  flex-flow: ${props => props.direction} wrap;
`;

const List = props => {
  return <Wrapper {...props}>{props.children}</Wrapper>;
};

List.defaultProps = {
  direction: "column"
};

List.propTypes = {
  direction: PropTypes.string.isRequired
};

export default List;
