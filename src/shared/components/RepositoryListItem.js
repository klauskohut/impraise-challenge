import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import moment from "moment";

import Box from "./Box";
import Icon from "./Icon";
import Text from "./Text";

const Wrapper = styled.div`
  display: flex;
  flex-flow: column;
  justify-content: space-between;
  padding: 20px;
  height: 120px;
  text-decoration: none;
  border: ${props => (props.type === "card" ? "1px solid #ebebeb" : "none")};
  margin-right: ${props => (props.type === "card" ? "20px" : "0")};
  margin-bottom: ${props => (props.type === "card" ? "20px" : "0")};
  border-bottom: ${props =>
    props.type === "card" ? "auto" : "1px solid #ebebeb"};
`;

const LanguageIcon = styled.span`
  border-radius: 50px;
  height: 10px;
  width: 10px;
  background-color: ${props => props.color};
`;

const Link = styled.a`
  flex: 1;
  min-width: 24%;
  text-decoration: none;
  &:hover > div {
    background-color: #f8f8f8;
  }
`;

const Top = styled.div`
  display: flex;
  flex-flow: column;
`;

const Bottom = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-start;
`;

const RepositoryListItem = props => {
  return (
    <Link href={props.url} alt={props.name}>
      <Wrapper {...props}>
        <Top>
          <Text size="24" color="#182a38">
            {props.name}
          </Text>
          <Text size="14" color="#182a38">
            {props.description}
          </Text>
        </Top>
        <Bottom>
          {props.language ? (
            <Box marginRight="10">
              <Text size="12" color="#848f9f">
                <LanguageIcon color={props.language.color} />
                &nbsp;
                {props.language.name}
              </Text>
            </Box>
          ) : null}
          {props.stargazersCount ? (
            <Box marginRight="10">
              <Text size="12" color="#848f9f">
                <Icon type="stargazer" />
                {props.stargazersCount}
              </Text>
            </Box>
          ) : null}
          {props.forksCount ? (
            <Box marginRight="10">
              <Text size="12" color="#848f9f">
                <Icon type="fork" />
                {props.forksCount}
              </Text>
            </Box>
          ) : null}
          {props.license ? (
            <Box marginRight="10">
              <Text size="12" color="#848f9f">
                <Icon type="law" />
                {props.license}
              </Text>
            </Box>
          ) : null}
          {props.updatedAt ? (
            <Box marginRight="10">
              <Text size="12" color="#848f9f">
                Updated on {moment(props.updatedAt).format("MMM Do")}
              </Text>
            </Box>
          ) : null}
        </Bottom>
      </Wrapper>
    </Link>
  );
};

RepositoryListItem.propTypes = {
  name: PropTypes.string.isRequired,
  updatedAt: PropTypes.string,
  description: PropTypes.string,
  stargazersCount: PropTypes.number.isRequired,
  forksCount: PropTypes.number.isRequired,
  language: PropTypes.shape({
    name: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired
  }),
  license: PropTypes.string
};

export default RepositoryListItem;
